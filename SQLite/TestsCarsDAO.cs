﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2_SQLite
{
    class TestsCarsDAO : ITestCarsDAO
    {
        static SQLiteConnection conn;
        public static string dataBase = @"C:\Users\LENOVO-1\Desktop\SQLite_Exam\CarsTests.db";
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static bool TestDbConnection(string conn)
        {
            try
            {
                using (var my_conn = new SQLiteConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to connect to database. Error : {ex}");
                my_logger.Error($"connection string: [{conn}]");
                return false;
            }
        }

        public void DeleteTests()
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"DELETE FROM Tests", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteCars()
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"DELETE FROM Cars", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }


        public void DeleteTestRecord(int id)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"DELETE FROM Tests WHERE ID = {id}", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteCarRecord(int id)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"DELETE FROM Cars WHERE ID = {id}", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void AddNewCar(Cars c)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"INSERT INTO Cars(Manufacturer,Model,Year)" +
                                                         $"VALUES('{c.manufacturer}', '{c.model}', {c.year})", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void AddNewTest(Tests t)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"INSERT INTO Tests(Car_ID, IsPassed, Date)" +
                                                        $"VALUES({t.carID}, {t.IsPassed}, '{t.date}')", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateRecordTest(int testID, Tests t)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"UPDATE Tests SET ID={t.ID}, CarID={t.carID}, IsPassed={t.IsPassed}, Date={t.date}"))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public List<Cars> GetAllCarsRecords()
        {
            List<Cars> car = new List<Cars>();
            using (SQLiteCommand cmd = new SQLiteCommand($"SELCT * FROM Cars", conn))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read() == true)
                    {
                        Cars c = new Cars
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            manufacturer = reader["Manufacturer"].ToString(),
                            model = reader["Model"].ToString(),
                            year = Convert.ToInt32(reader["Year"])
                        };
                        car.Add(c);
                    }
                }
            }
            return car;
        }

        public List<Tests> GetAllTestsRecords()
        {
            List<Tests> test = new List<Tests>();
            using (SQLiteCommand cmd = new SQLiteCommand($"SELCT * FROM Tests", conn))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        Tests t = new Tests
                        {
                            carID = Convert.ToInt32(reader["Car_ID"]),
                            IsPassed = (bool)reader["IsPassed"],
                            date = (DateTime)reader["Date"]
                        };
                        test.Add(t);
                    }
                }
            }
            return test;
        }

        public void GetAllRecords()
        {
            List<Cars> car = GetAllCarsRecords();
            List<Tests> test = GetAllTestsRecords();

            var result = (from c in car
                          join t in test on c.ID equals t.carID
                          select new {
                          c.manufacturer,
                          c.model,
                          c.year,
                          t.date,
                          t.IsPassed
                          }).ToString();
            Console.WriteLine(result);
        }

        public Cars GetCarsByManufacturer(string manuf)
        {
            using (SQLiteCommand cmd = new SQLiteCommand($"SELCT * FROM Cars WHERE Manufacturer = {manuf}", conn))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read() == true)
                    {
                        Cars c = new Cars
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            manufacturer = reader["Manufacturer"].ToString(),
                            model = reader["Model"].ToString(),
                            year = Convert.ToInt32(reader["Year"])
                        };
                        return c;
                    }
                }
            }
            return null;
        }
    }
}
