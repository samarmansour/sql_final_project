﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2_SQLite
{
    interface ITestCarsDAO
    {
        List<Cars> GetAllCarsRecords();
        List<Tests> GetAllTestsRecords();
        void GetAllRecords();
        void AddNewCar(Cars c);
        void AddNewTest(Tests t);
        void UpdateRecordTest(int testID, Tests t);
        void DeleteTests();
        void DeleteCars();
        void DeleteTestRecord(int id);
        void DeleteCarRecord(int id);
        Cars GetCarsByManufacturer(string manuf);

    }
}
