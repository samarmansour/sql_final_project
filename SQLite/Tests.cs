﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2_SQLite
{
    class Tests
    {
        public int ID { get; set; }
        public int carID { get; set; }
        public bool IsPassed { get; set; }
        public DateTime date { get; set; }

        public static bool operator ==(Tests t1, Tests t2)
        {
            if ((t1 == null) && (t2 == null))
                return true;
            if ((t1 == null) || (t2 == null))
                return false;

            return (t1.ID == t2.ID);
        }
        public static bool operator !=(Tests t1, Tests t2)
        {
            return !(t1 == t2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Tests t = obj as Tests;
            if (t == null)
                return false;

            return this.ID == t.ID;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
