﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2_SQLite
{
    class Cars
    {
        public int ID { get; set; }
        public string manufacturer { get; set; }
        public string model { get; set; }
        public int year { get; set; }

        public static bool operator ==(Cars c1, Cars c2)
        {
            if ((c1 == null) && (c2 == null))
                return true;
            if ((c1 == null) || (c2 == null))
                return false;

            return (c1.ID == c2.ID);
        }
        public static bool operator !=(Cars c1, Cars c2)
        {
            return !(c1 == c2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Cars c = obj as Cars;
            if (c == null)
                return false;

            return this.ID == c.ID;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
