CREATE TABLE "Cars" (
	"ID"	INTEGER,
	"Manufacturer"	TEXT NOT NULL,
	"Model"	TEXT NOT NULL,
	"Year"	INTEGER,
	PRIMARY KEY("ID" AUTOINCREMENT)
);

CREATE TABLE "Tests" (
	"ID"	INTEGER,
	"Car_ID"	INTEGER NOT NULL,
	"IsPassed"	NUMERIC,
	"Test_Date"	datetime,
	PRIMARY KEY("ID" AUTOINCREMENT),
	FOREIGN KEY("Car_ID") REFERENCES "Cars"("ID")
);

INSERT INTO Cars(Manufacturer,Model,Year)
VALUES('Toyota','Rav4',2019);
INSERT INTO Cars(Manufacturer,Model,Year)
VALUES('Audi','Q8',2021);

INSERT INTO Tests(Car_ID,IsPassed,Test_Date)
VALUES(1,0,2022-11-12);
INSERT INTO Tests(Car_ID,IsPassed,Test_Date)
VALUES(1,1,2019-05-06);