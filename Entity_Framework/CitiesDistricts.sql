USE [CitiesDistricts]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 12/31/2020 9:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[District_ID] [bigint] NULL,
	[Mayor] [varchar](255) NOT NULL,
	[Population] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Districts]    Script Date: 12/31/2020 9:32:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Districts](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Population] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([ID], [Name], [District_ID], [Mayor], [Population]) VALUES (1, N'CALIFORNIA', 1, N'BELLY', NULL)
INSERT [dbo].[Cities] ([ID], [Name], [District_ID], [Mayor], [Population]) VALUES (2, N'NEW YORK', 1, N'MIKE', NULL)
INSERT [dbo].[Cities] ([ID], [Name], [District_ID], [Mayor], [Population]) VALUES (3, N'MEMPHIS', 2, N'ROGER', NULL)
INSERT [dbo].[Cities] ([ID], [Name], [District_ID], [Mayor], [Population]) VALUES (4, N'SAN FRANCISCO', 4, N'PETTER', NULL)
SET IDENTITY_INSERT [dbo].[Cities] OFF
SET IDENTITY_INSERT [dbo].[Districts] ON 

INSERT [dbo].[Districts] ([ID], [Name], [Population]) VALUES (1, N'North', 60000000)
INSERT [dbo].[Districts] ([ID], [Name], [Population]) VALUES (2, N'South', 5000000)
INSERT [dbo].[Districts] ([ID], [Name], [Population]) VALUES (3, N'East', 750000)
INSERT [dbo].[Districts] ([ID], [Name], [Population]) VALUES (4, N'West', 5000000)
SET IDENTITY_INSERT [dbo].[Districts] OFF
ALTER TABLE [dbo].[Cities]  WITH CHECK ADD FOREIGN KEY([District_ID])
REFERENCES [dbo].[Districts] ([ID])
GO
