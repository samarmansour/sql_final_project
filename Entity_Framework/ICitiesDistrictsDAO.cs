﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part4_EF
{
    interface ICitiesDistrictsDAO
    {
        void AddCity(string name, int districtID, string mayor);
        void DeleteCityRecords();
        void DeleteDistrictRecords();
        void UpdateCitiesMayor(int id, string name);
        void GetMapCitiesToDistricts();
        City GetCityByID(int id);
        District GetDistricsByID(int id);

    }
}
