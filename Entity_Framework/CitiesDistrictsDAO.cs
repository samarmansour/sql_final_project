﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part4_EF
{
    class CitiesDistrictsDAO : ICitiesDistrictsDAO
    {
        public void AddCity(string name, int districtID, string mayor)
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                var c = new City()
                {
                    Name = name,
                    District_ID = districtID,
                    Mayor = mayor
                };
                cd.Cities.Add(c);

                cd.SaveChanges();
            }
        }

        public void DeleteCityRecords()
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                cd.Database.ExecuteSqlCommand("DELETE FROM Cities");
                cd.SaveChanges();
            }
        }

        public void DeleteDistrictRecords()
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                cd.Database.ExecuteSqlCommand("DELETE FROM Disticts");
                cd.SaveChanges();
            }
        }

        public City GetCityByID(int id)
        {
            City c = null;
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
               c =  cd.Cities.First(_ => _.ID == id);
            }
            return c;
        }

        public District GetDistricsByID(int id)
        {
            District d = null;
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                d = cd.Districts.First(_ => _.ID == id);
            }
            return d;
        }

        public void GetMapCitiesToDistricts()
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                var data = cd.Cities.Join(
                                            cd.Districts,
                                            city => city.District_ID,
                                            district => district.ID,
                                            (city, district) => new
                                            {
                                                CityDisID = city.District_ID,
                                                distID = district.ID
                                            }).ToString();
            }
        }
        public void UpdateCitiesMayor(int id, string name)
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                City c = cd.Cities.Where(f => f.ID == id).FirstOrDefault();
                if (c == null) throw new Exception("something went wrong!");
                c.Mayor = name;
                cd.SaveChanges();
            }
        }

        public void GetCitiesByPopulation(int pop)
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                var max = cd.Cities.Max(c => c.Population > pop);
            }
        }

        public void UpdateCities(int id, District dis)
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                City c = cd.Cities.Where(f => f.ID == id & f.District_ID == dis.ID).FirstOrDefault();
                if (c == null) throw new Exception("something went wrong!");
                c.Population = dis.Population;
                var sum = cd.Cities.Sum(city => city.Population);
                cd.SaveChanges();
            }
        }

        public void GetAllCitiesQuerySyntax()
        {
            using (CitiesDistrictsEntities cd = new CitiesDistrictsEntities())
            {
                var all = from c in cd.Cities select c;
            }
        }

    }
}
