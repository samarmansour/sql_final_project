﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3_SQLServer
{
    class Stores
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int floor { get; set; }
        public int categoryID { get; set; }

        public static bool operator ==(Stores s1, Stores s2)
        {
            if ((s1 == null) && (s2 == null))
                return true;
            if ((s1 == null) || (s2 == null))
                return false;

            return (s1.ID == s2.ID);
        }
        public static bool operator !=(Stores s1, Stores s2)
        {
            return !(s1 == s2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Stores s = obj as Stores;
            if (s == null)
                return false;

            return this.ID == s.ID;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
