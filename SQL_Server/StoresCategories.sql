USE [StoresCategories]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 12/31/2020 1:49:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stores]    Script Date: 12/31/2020 1:49:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stores](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[StoreFloor] [int] NOT NULL,
	[Category_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([ID], [NAME]) VALUES (1, N'ELECTRICS')
INSERT [dbo].[Categories] ([ID], [NAME]) VALUES (2, N'FASHION')
INSERT [dbo].[Categories] ([ID], [NAME]) VALUES (3, N'FOOD')
INSERT [dbo].[Categories] ([ID], [NAME]) VALUES (4, N'GAMES')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Stores] ON 

INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (1, N'SAMSUNG', 3, 1)
INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (2, N'CITY DEAL', 3, 1)
INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (3, N'GUCCI', 5, 2)
INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (4, N'ZARA', 5, 2)
INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (5, N'ITALIANO', 1, 3)
INSERT [dbo].[Stores] ([ID], [Name], [StoreFloor], [Category_ID]) VALUES (6, N'RACING', 4, 4)
SET IDENTITY_INSERT [dbo].[Stores] OFF
ALTER TABLE [dbo].[Stores]  WITH CHECK ADD FOREIGN KEY([Category_ID])
REFERENCES [dbo].[Categories] ([ID])
GO
