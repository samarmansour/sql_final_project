﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3_SQLServer
{
    interface IStoresCategoriesDAO
    {
        void DeleteAllRecorde();
        void DeleteStoreRecord(int id);
        void DeleteCategoriesRecord(int id);
        void AddStore(Stores s);
        void AddCategory(Categories c);
        int UpdateStoreRecord(int id, Stores s);
        Stores GetStoreByID(int id);
        Categories GetCategoryByID(int id);
        List<Categories> GetAllCategoryRecords();
        List<Stores> GetAllStoreRecords();


    }
}
