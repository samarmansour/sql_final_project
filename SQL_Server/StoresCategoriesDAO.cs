﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part3_SQLServer
{
    class StoresCategoriesDAO : IStoresCategoriesDAO
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string m_connString;

        static bool TestDbConnection(string conn)
        {
            try
            {
                using (var my_conn = new SqlConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to connect to database! Error : {ex}");
                my_logger.Error($"Connection Sring: [{conn}]");
                return false;
            }
        }

        public StoresCategoriesDAO(string conn)
        {
            m_connString = conn;
        }

        private Stores ExecuteNonQueryStores(string query)
        {
            Stores store = null;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read() == true)
                    {
                        store = new Stores
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            name = reader["Name"].ToString(),
                            floor = Convert.ToInt32(reader["StoreFloor"]),
                            categoryID = Convert.ToInt32(reader["Category_ID"])
                        };
                    }
                }
            }
            return store;
        }

        private Categories ExecuteNonQueryCategory(string query)
        {
            Categories category = null;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read() == true)
                    {
                        category = new Categories
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            Name = reader["Name"].ToString()
                        };
                    }
                }
            }
            return category;
        }

        private int ExecuteNonQuery(string query)
        {
            int result = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }
        public void AddCategory(Categories c)
        {
            ExecuteNonQuery($"INSERT INTO Categories(Name) VALUES('{c.Name}')");

        }

        public void AddStore(Stores s)
        {
            ExecuteNonQuery($"INSERT INTO Stores(Name,StoreFloor,Category_ID) VALUES('{s.name}', '{s.floor}', {s.categoryID})");
        }

        public void DeleteAllRecorde()
        {
            ExecuteNonQuery($"DELETE FROM Categories");
        }

        public void DeleteCategoriesRecord(int id)
        {
            ExecuteNonQuery($"DELETE FROM Categories WHERE ID ={id}");
        }

        public void DeleteStoreRecord(int id)
        {
            ExecuteNonQuery($"DELETE FROM Stores WHERE ID ={id}");
        }

        public List<Categories> GetAllCategoryRecords()
        {
            List<Categories> category = new List<Categories>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Categories"))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Categories c = new Categories
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            Name = reader["Name"].ToString()
                        };
                        category.Add(c);
                    }
                }
            }
            return category;
        }

        public List<Stores> GetAllStoreRecords()
        {
            List<Stores> store = new List<Stores>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Stores"))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Stores s = new Stores
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            name = reader["Name"].ToString(),
                            floor = Convert.ToInt32(reader["StoreFloor"]),
                            categoryID = Convert.ToInt32(reader["Category_ID"])
                        };
                        store.Add(s);
                    }
                }
            }
            return store;
        }

        public Categories GetCategoryByID(int id)
        {
            return ExecuteNonQueryCategory($"SELECT * FROM Categories WHERE ID={id}");
        }

        public Stores GetStoreByID(int id)
        {
            return ExecuteNonQueryStores($"SELECT * FROM Stores WHERE ID={id}");
        }

        public int UpdateStoreRecord(int id, Stores s)
        {
            int result = ExecuteNonQuery($"UPDATE FROM Stores" +
                                        $"SET Name = '{s.name}', StoreFloor = {s.floor}, Category_ID = {s.categoryID}" +
                                        $"WHERE ID = {id}");
            return result;
        }

        public object GetAllStoresByCategoryAndFloor(int s_floor, string c_name)
        {
            return ExecuteNonQuery($"SELECT c.Name, c.ID, Count(c.Name) AS maxRepeatedCategory FROM Categories c " +
                                    $"JOIN Stores s ON s.Category_ID = c.ID" +
                                    $"WHERE s.StoerFloor = {s_floor} AND c.Name = {c_name}");
        }

        public object GetAllMaxStoresByCategories()
        {
            return ExecuteNonQuery($"SELECT c.Name, c.ID, Count(c.Name) AS maxRepeatedCategory FROM Categories c " +
                                   $"JOIN Stores s ON s.Category_ID = c.ID" +
                                   $"GROUP BY c.Name, c.ID" +
                                   $"Having(Count(c.Name) > 1)");
        }
    }
}
