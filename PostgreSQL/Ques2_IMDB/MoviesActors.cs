﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    class MoviesActors : IEquatable<MoviesActors>
    {
        public int ID { get; set; }
        public int actorID { get; set; }
        public int movieID { get; set; }

        public static bool operator ==(MoviesActors m1, MoviesActors m2)
        {
            if ((m1 == null) && (m2 == null))
                return true;
            if ((m1 == null) || (m2 == null))
                return false;

            return (m1.ID == m2.ID);
        }
        public static bool operator !=(MoviesActors m1, MoviesActors m2)
        {
            return !(m1 == m2);
        }

        public bool Equals(MoviesActors movie)
        {
            if (movie == null)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
