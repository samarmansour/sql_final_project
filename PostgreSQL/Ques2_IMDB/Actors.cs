﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    class Actors : IEquatable<Actors>
    {
        public int ID { get; set; }
        public string name { get; set; }
        public DateTime birthDate { get; set; }

        public static bool operator ==(Actors a1, Actors a2)
        {
            if ((a1 == null) && (a2 == null))
                return true;
            if ((a1 == null) || (a2 == null))
                return false;

            return (a1.ID == a2.ID);
        }
        public static bool operator !=(Actors a1, Actors a2)
        {
            return !(a1 == a2);
        }

        public bool Equals(Actors actor)
        {
            if (actor == null)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }
        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
