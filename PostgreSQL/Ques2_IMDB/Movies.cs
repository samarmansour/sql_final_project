﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    class Movies: IEquatable<Movies>
    {
        public int ID { get; set; }
        public string name { get; set; }
        public DateTime releaseDate { get; set; }
        public int genreID { get; set; }

        public static bool operator ==(Movies m1, Movies m2)
        {
            if ((m1 == null) && (m2 == null))
                return true;
            if ((m1 == null) || (m2 == null))
                return false;

            return (m1.ID == m2.ID);
        }
        public static bool operator !=(Movies m1, Movies m2)
        {
            return !(m1 == m2);
        }

        public bool Equals(Movies movie)
        {
            if (movie == null)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }
        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
