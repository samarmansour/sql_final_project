﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    class Genres:IEquatable<Genres>
    {
        public int ID { get; set; }
        public string name { get; set; }

        public static bool operator ==(Genres g1, Genres g2)
        {
            if ((g1 == null) && (g2 == null))
                return true;
            if ((g1 == null) || (g2 == null))
                return false;

            return (g1.ID == g2.ID);
        }
        public static bool operator !=(Genres g1, Genres g2)
        {
            return !(g1 == g2);
        }

        public bool Equals(Genres genre)
        {
            if (genre == null)
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return this.ID;
        }
        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
