﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    interface IWebIMDBDAO
    {
        void DeleteMovies(NpgsqlConnection conn);
        void UpdateMovieReleaseDate(int id, Movies movie, NpgsqlConnection conn);
        void AddNewMovie(Movies movie, NpgsqlConnection conn);
        List<Movies> GetAllRecoredMovies(string conn_string, string query);
        Genres GetGenresByID(int id, string conn_string);
    }
}
