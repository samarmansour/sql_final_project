﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_IMDB
{
    class WebIMDBDAO : IWebIMDBDAO
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private bool  GetOpenConnection(string conn)
        {
            try
            {
                using (var my_conn = new NpgsqlConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed! can connect to database: {ex}");
                return false;
            }

        }

        public List<Movies> GetAllRecoredMovies(string conn_string, string query)
        {
            using (var conn = new NpgsqlConnection(conn_string))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;

                var reader = cmd.ExecuteReader();
                List<Movies> m = new List<Movies>();
                while (reader.Read())
                {
                    m.Add(
                        new Movies
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            name = reader["Name"].ToString(),
                            releaseDate = Convert.ToDateTime(reader["ReleaseDate"]),
                            genreID = Convert.ToInt32(reader["Genre_ID"])
                        });
                }
                return m;
            }

        }

        public Genres GetGenresByID(int id, string conn_string)
        {
            using (var conn = new NpgsqlConnection(conn_string))
            {
                conn.Open();
                string query = $"SELECT * FROM genres WHERE ID = {id}";
                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;

                var reader = cmd.ExecuteReader();
                Genres genres = new Genres();
                while (reader.Read())
                {
                    genres =
                        new Genres
                        {
                            ID = Convert.ToInt32(reader["ID"]),
                            name = reader["Name"].ToString()
                        };
                }
                return genres;
            }
        }
        public void AddNewMovie(Movies movie, NpgsqlConnection conn)
        {
            string insertQuery = $"INSERT INTO movies(Name, ReleaseDate, Genre_ID)" +
                                  $"VALUES ('{movie.name}', '{movie.releaseDate.ToString("yyy-MM-dd")}', {movie.genreID});";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(insertQuery, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                Decimal result = Convert.ToDecimal(cmd.ExecuteScalar());
                my_logger.Info($"New movie record {movie.name} was inserted with ID {result}");
                movie.ID = (int)result;
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to insert new movie into database. Error : {ex}");
                my_logger.Error($"InsertNewMovie: [{insertQuery}]");
            }

        }

        public void DeleteMovies(NpgsqlConnection conn)
        {
            string deleteQuery = $"DELETE FROM movies;";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(deleteQuery, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                my_logger.Info($"Deleted All Movie records");
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to delete movie records. Error : {ex}");
                my_logger.Error($"DeleteMovie: [{deleteQuery}]");
            }
        }

        public void UpdateMovieReleaseDate(int id, Movies movie, NpgsqlConnection conn)
        {
            string UpdateQuery = $"Update movies SET ReleaseDate = '{movie.releaseDate.ToString("yyy-MM-dd")}'" +
                                    $"WHERE ID = {id};";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(UpdateQuery, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                my_logger.Info($"Update movie release date records");
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to update movie records. Error : {ex}");
                my_logger.Error($"UpdateReleasedateMovie: [{UpdateQuery}]");
            }
        }

        public void GetMovieRecordByActor(NpgsqlConnection conn)
        {
            string query = $"select a.Name,m.Name, a.BirthDate  from movies m " +
                           $"join movies_actors ma on m.ID = ma.MovieID" +
                           $"join actors a on ma.ActorID = a.ID" +
                           $"where ma.MovieID = m.ID and(SELECT extract(year from a.BirthDate)::int < 1972);";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                my_logger.Info($"Actor birthdate");
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to get a records. Error : {ex}");
                my_logger.Error($"Actor/Actress Birth date: [{query}]");
            }
        }

        public void GetFirstMovieReleasedYearly(NpgsqlConnection conn)
        {
            string query = $"select date_trunc('year', ReleaseDate)::date, ReleaseDate from movies order by 1;";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                my_logger.Info($"Release date");
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to get a records. Error : {ex}");
                my_logger.Error($"Release date: [{query}]");
            }
        }

        public void GetMaxActorRoles(NpgsqlConnection conn)
        {
            string query = $"SELECT a.Name ,count(*) moviesRole  from movies m" +
                            $"JOIN movies_actors ma on m.ID = ma.MovieID" +
                            $"JOIN actors a on ma.ActorID = a.ID" +
                            $"WHERE MovieID = m.ID" +
                            $"GROUP BY a.Name ORDER BY moviesRole DESC Limit 1; ";
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                my_logger.Info($"Leading actor/actress roles");
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to get a records. Error : {ex}");
                my_logger.Error($"HighestMovieRoles: [{query}]");
            }
        }
    }
}
