--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: random_between(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.random_between(low_salary integer, hight_salary integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return floor(random() * (Hight_salary - low_salary + 1) + low_salary)::integer;
    end;
    $$;


ALTER FUNCTION public.random_between(low_salary integer, hight_salary integer) OWNER TO postgres;

--
-- Name: sp_add_500_to_each_worker(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_add_500_to_each_worker() RETURNS TABLE(worker_name text, new_salary integer)
    LANGUAGE plpgsql
    AS $$
    DECLARE
        bouns int = 500;
    BEGIN
        FOR row IN 1..(SELECT count(*) FROM workers) LOOP
            UPDATE workers
            SET Salary = Salary + bouns
            where ID = row;
        end loop;
        RETURN query (SELECT Name, Salary FROM workers);
    end;
    $$;


ALTER FUNCTION public.sp_add_500_to_each_worker() OWNER TO postgres;

--
-- Name: sp_add_salary(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_add_salary() RETURNS TABLE(workername text, phone text, salary integer, role text)
    LANGUAGE plpgsql
    AS $$
    DECLARE
        rID bigint := (select RoleID from workers LIMIT 1)::integer;
    BEGIN
        return query
        SELECT w.Name, p.PhoneNumber, w.Salary, r.Name FROM workers w
        JOIN roles r  on r.ID = w.RoleID
        JOIN phones p on w.PhoneID = p.ID;
            IF rID::int = 1 then
                Update workers
                set salary = 20000 where RoleID = 1;
            else
                Update workers
                set salary = random_between(5000, 10000)
                where RoleID != 1;
            end if;
    end;
    $$;


ALTER FUNCTION public.sp_add_salary() OWNER TO postgres;

--
-- Name: sp_average_salary_by_siteid(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_average_salary_by_siteid() RETURNS TABLE(avg_workernumbers bigint, name text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT (avg(w.ID))::bigint workers_number, s.Name FROM workers w
        JOIN sites s on w.SiteID = s.ID
        group by w.SiteID, s.Name;
    end;
    $$;


ALTER FUNCTION public.sp_average_salary_by_siteid() OWNER TO postgres;

--
-- Name: sp_avg_salary(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_avg_salary(_workerroleid bigint) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
        average_salary float;
    BEGIN
        SELECT avg(Salary) into average_salary from workers
        where RoleID = _workerRoleID;

        return average_salary;
    end;
    $$;


ALTER FUNCTION public.sp_avg_salary(_workerroleid bigint) OWNER TO postgres;

--
-- Name: sp_site_worker_number(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_site_worker_number() RETURNS TABLE(site text, workersnumbers bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
       return query
        SELECT s.Name, count(w.ID) workerNumber FROM sites s
        JOIN workers w on s.ID = w.SiteID
        GROUP BY s.Name, w.ID order by workerNumber DESC limit 1;
    end;
    $$;


ALTER FUNCTION public.sp_site_worker_number() OWNER TO postgres;

--
-- Name: sp_workers_by_id(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_workers_by_id(_siteid bigint) RETURNS TABLE(worker_name text, phone text, salary integer, role text, site_name text, site_address text)
    LANGUAGE plpgsql
    AS $$
    Begin
        return query
        SELECT w.Name, p.PhoneNumber, w.Salary, r.Name, s.Name, s.Address From sites s
        JOIN workers w on w.SiteID = s.ID
        JOIN roles r on w.RoleID = r.ID
        JOIN phones p on w.PhoneID = p.ID
        WHERE _SiteID = s.ID;
    end;
    $$;


ALTER FUNCTION public.sp_workers_by_id(_siteid bigint) OWNER TO postgres;

--
-- Name: sp_workers_roles(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_workers_roles() RETURNS TABLE(name text, phone text, salary integer, role text, siteid bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT w.NAME, p.PhoneNumber,w.Salary,r.Name,w.SiteID from workers w
        JOIN roles r on w.RoleID = r.ID
        JOIN phones p ON p.ID = w.PhoneID;
    end;
    $$;


ALTER FUNCTION public.sp_workers_roles() OWNER TO postgres;

--
-- Name: sp_workers_to_new_site(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_workers_to_new_site(_siteid bigint) RETURNS TABLE(workername text, phone text, new_site text)
    LANGUAGE plpgsql
    AS $$
    DECLARE
        rand_rowSiteID bigint = (Select ID from sites order by random() LIMIT 1);
    BEGIN
        return query
        SELECT w.Name, p.PhoneNumber, s.Name FROM workers w
        JOIN phones p on w.PhoneID = p.ID
        JOIN sites s on w.SiteID = s.ID
        where s.ID = _siteID;
        if (SELECT ID From sites limit 1) = _siteID and rand_rowSiteID <> _siteID THEN
            update sites SET ID = rand_rowSiteID where _siteID = ID;
            DELETE FROM sites WHERE _siteID = ID;
        else
            raise notice 'Current value of, _siteID not founded!';
        end if;
    end;
    $$;


ALTER FUNCTION public.sp_workers_to_new_site(_siteid bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: phones; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phones (
    id integer NOT NULL,
    phonenumber text
);


ALTER TABLE public.phones OWNER TO postgres;

--
-- Name: phones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.phones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.phones_id_seq OWNER TO postgres;

--
-- Name: phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.phones_id_seq OWNED BY public.phones.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sites (
    id integer NOT NULL,
    name text NOT NULL,
    address text NOT NULL
);


ALTER TABLE public.sites OWNER TO postgres;

--
-- Name: sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sites_id_seq OWNER TO postgres;

--
-- Name: sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sites_id_seq OWNED BY public.sites.id;


--
-- Name: workers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workers (
    id integer NOT NULL,
    name text NOT NULL,
    phoneid bigint NOT NULL,
    salary integer,
    roleid bigint NOT NULL,
    siteid bigint NOT NULL
);


ALTER TABLE public.workers OWNER TO postgres;

--
-- Name: workers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.workers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workers_id_seq OWNER TO postgres;

--
-- Name: workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.workers_id_seq OWNED BY public.workers.id;


--
-- Name: phones id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phones ALTER COLUMN id SET DEFAULT nextval('public.phones_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: sites id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sites ALTER COLUMN id SET DEFAULT nextval('public.sites_id_seq'::regclass);


--
-- Name: workers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers ALTER COLUMN id SET DEFAULT nextval('public.workers_id_seq'::regclass);


--
-- Data for Name: phones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.phones (id, phonenumber) FROM stdin;
1	0563214879
2	0533326987
3	05021489036
4	0452369871
5	0552309871
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
1	Driver
2	Builder
3	Engineer
\.


--
-- Data for Name: sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sites (id, name, address) FROM stdin;
1	Blue Bird	La Mesa 27, LA
2	Shallow	La Mesa 27, LA
3	De La Cruze	Diego 2752, TX
\.


--
-- Data for Name: workers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.workers (id, name, phoneid, salary, roleid, siteid) FROM stdin;
1	Juan	3	20500	1	3
2	Pablo	4	20500	1	3
3	Louis	2	6506	2	3
4	Louis	2	10319	2	2
5	Markos	1	8912	2	1
6	Ricky	5	7578	3	1
7	Ricky	5	9067	3	2
8	Ricky	5	7977	3	3
\.


--
-- Name: phones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.phones_id_seq', 5, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- Name: sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sites_id_seq', 3, true);


--
-- Name: workers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.workers_id_seq', 8, true);


--
-- Name: phones phones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phones
    ADD CONSTRAINT phones_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sites sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- Name: workers workers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_pkey PRIMARY KEY (id);


--
-- Name: workers workers_phoneid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_phoneid_fkey FOREIGN KEY (phoneid) REFERENCES public.phones(id);


--
-- Name: workers workers_roleid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(id);


--
-- Name: workers workers_siteid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workers
    ADD CONSTRAINT workers_siteid_fkey FOREIGN KEY (siteid) REFERENCES public.sites(id);


--
-- PostgreSQL database dump complete
--

