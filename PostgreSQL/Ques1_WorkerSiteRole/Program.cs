﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part5_PostgreSQL_WorkerSiteRole
{
    class Program
    {
        static bool TestDbConnection(string conn)
        {
            try
            {
                using (var my_conn = new NpgsqlConnection(conn))
                {
                    my_conn.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to connect to data base. Error : {ex}");
                return false;
            }
        }
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static List<Dictionary<string, object>> Run_Any_sp(string conn_string, string sp_name,
             NpgsqlParameter[] parameters)
        {
            List<Dictionary<string, object>> values = new List<Dictionary<string, object>>();

            try
            {
                using (var conn = new NpgsqlConnection(conn_string))
                {
                    conn.Open();

                    NpgsqlCommand command = new NpgsqlCommand(sp_name, conn);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.Add(parameters);

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Dictionary<string, object> one_row = new Dictionary<string, object>();
                        foreach (var item in reader.GetColumnSchema())
                        {
                            object column_value = reader[item.ColumnName];
                            one_row.Add(item.ColumnName, column_value);
                        }
                        values.Add(one_row);
                    }
                }
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed! database record not exist. Error : {ex}");
                Console.WriteLine($"Function {sp_name} failed. parameters: {string.Join(",", parameters.Select(_ => _.ParameterName + " : " + _.Value))}");
            }
            return values;
        }


        static void Main(string[] args)
        {
            string m_conn = "Host=localhost;Username=postgres;Password=MansorySam1993$$;Database=CompanySite";

            var res_sp_random = Run_Any_sp(m_conn, "sp_avg_salary", new NpgsqlParameter[]
                {
                    new NpgsqlParameter("_workerRoleID",3)
                });
            Console.WriteLine($"Run sp of sp avgerage salary. result = {res_sp_random[0]["sp_avg_salary"]}");
            Console.WriteLine(res_sp_random.Count);
            

        }
    }
}
